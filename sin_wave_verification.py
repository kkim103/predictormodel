import model
import torch
import tcav
from mnist import MNIST
import numpy as np
import data_handler as dh
import matplotlib.pyplot as plt

def generate_sin_wave(duration, sample_size, amplitude, frequency, phase):
    indices = np.linspace(0,duration, sample_size)
    indices *= 2*np.pi*frequency
    indices += phase
    return np.sin(indices) * amplitude

def sample_trace(trace, duration, sample_count):
    output = []
    for i in range(sample_count):
        offset = np.random.randint(0,len(trace)-duration-1)
        output.append(trace[offset:offset+duration])
    return output

def combine_sin_wave(array_of_sin, active_wave=False):
    output = []
    active = []
    for i in range(len(array_of_sin[0])):
        active_ind = []
        value = 0
        for n in range(len(array_of_sin)):
            v = array_of_sin[n][i]
            value += v
            active_ind.append(v)
        is_same = True
        e = active_ind[0]
        for i in active_ind:
            if i != e :
                is_same = False
        if active_wave:
            if is_same and len(active_ind) != 1:
                active.append(0)
            elif len(active_ind) == 1:
                active.append(1 if active_ind[0]>0 else 0)
            else:
                active.append(np.argmax(active_ind)+1)
        output.append(value)
    return output, active

# output_active outputs 1 or 0 depending on the wave's signal value being positive or negative
def output_active(wave):
    output = []
    for v in wave:
        if v > 0:
            output.append(1)
        else:
            output.append(0)
    return output


@torch.no_grad()
def get_sin_dataset(dataset, time_steps, offset, labels=None):
    list_of_examples = []
    list_of_labels = []
    for i in range(len(dataset)):
        if i + time_steps+offset>= len(dataset):
            break

        # squash the mutiple steps
        example_datum = torch.zeros((time_steps))
        for j in range(time_steps):
            example_datum[j] = dataset[j+i]
        list_of_examples.append(example_datum)
        if labels != None:
            label_datum = torch.zeros((time_steps)).int()
            for j in range(time_steps):
                label_datum[j] = int(labels[i + offset + j])
            list_of_labels.append(label_datum)
        else:
            label_datum = torch.zeros((time_steps))
            for j in range(time_steps):
                label_datum[j] = dataset[i + offset + j]
            list_of_labels.append(label_datum)


    return list_of_examples, list_of_labels

if __name__ == "__main__":

    timesteps = 100
    x = np.linspace(0, 100, 9000)
    amp1 = 1
    amp2 = 2
    freq1 = 1
    freq2 = 0.3
    phase1 = 0 # in rad
    phase2 = np.pi

    need_train = True

    if need_train:
        dataset1 = generate_sin_wave(1000, 9000, amp1, freq1, phase1)
        #dataset2 = generate_sin_wave(1000, 9000, amp2, freq2, phase2)
        dataset, labels = combine_sin_wave([dataset1], active_wave=True)
        XS, YS = get_sin_dataset(dataset, 1, 0, labels=labels)
        print(XS[:10])
        print(YS[:10])
        
        param = {}
        param["nlayers"] = 2
        param["input_dim"] = 1
        param["output_dim"] = 3
        param["hidden_dim"] = 100
        param["epoch"] = 10
        param["path"] = "./tmp"
        param["timesteps"] = timesteps
        param["loss"]="cce"
        param["classification"]=True


        # train happens here
        predictor = model.seq_kfold_validation(2, XS, YS, False, param)
        predictor.cuda()

    # Test each concepts
    ''' frequency concept. Keep other variables (i.e. phase and amp) constant '''
    delta1 = freq1 * 0.05 # freuqnecy that falls within 0.001 will be considered positive
    delta2 = freq2 * 0.05 # freuqnecy that falls within 0.001 will be considered positive
    # for frequency 1
    neg_frequency_concepts = []
    labels = []
    i = 0
    while i < 100:
        i+= 1
        f = np.random.uniform(0, 100)
        if abs(freq1-f) < delta1:
            i -= 1
            continue
        neg_frequency_concepts.append(f)

    pos_frequency_concepts = []
    i = 0
    while i < 100:
        i+=1
        f = np.random.uniform(freq1 - delta1, freq1 + delta1)
        pos_frequency_concepts.append(f)

    neg_traces = []
    for t in neg_frequency_concepts:
        t = generate_sin_wave(1000,9000,amp1,t, phase1)
        neg_traces += sample_trace(t, timesteps, 100)

    pos_traces = []
    for t in pos_frequency_concepts:
        t = generate_sin_wave(1000,9000,amp1,t, phase1)
        pos_traces += sample_trace(t, timesteps, 100)
    print("trace length (neg, pos): (%d,%d)"% (len(neg_traces),len(pos_traces)))
    neg_bottle = []
    pos_bottle = []
    with torch.no_grad():
        for s in neg_traces:
            predictor.init_hidden()
            x = torch.tensor(s).view((timesteps,1,1)).float().cuda()
            bottle, _ = predictor.output_bottleneck(x)
            neg_bottle.append(bottle.squeeze()[-1])
            # reset hidden state
        for s in pos_traces:
            predictor.init_hidden()
            x = torch.tensor(s).view((timesteps,1,1)).float().cuda()
            bottle, _ = predictor.output_bottleneck(x)
            pos_bottle.append(bottle.squeeze()[-1])
            # reset hidden state
            #predictor.init_hidden()
        print("bottle length (neg, pos): (%d,%d)"% (len(neg_bottle),len(pos_bottle)))

    permutation = np.random.permutation(len(neg_bottle) + len(pos_bottle))
    comb_bottle = neg_bottle + pos_bottle
    if not torch.all(torch.eq(comb_bottle[0], neg_bottle[0])):
        print("ERROR: TCAV PERMUTATION MAY NOT BE CORRECT")
        exit()
    cav_x = []
    cav_y = []
    for i in range(len(neg_bottle)+len(pos_bottle)):
        p = permutation[i]
        y = 0
        if p >= len(neg_bottle):
            y = 1
        cav_y.append(y)
        cav_x.append(comb_bottle[p])
    cav_train_x = cav_x[:int(len(cav_x)*0.75)]
    cav_train_y = cav_y[len(cav_train_x):]
    cav_test_x = cav_x[int(len(cav_x)*0.75):]
    cav_test_y = cav_y[len(cav_test_x):]
    print(len(cav_train_x))
    cav_model = tcav._linear_model(len(cav_train_x[0]), 2, cav_train_x, cav_train_y)
    accuracy = tcav.benchmark(cav_model, cav_test_x, cav_test_y)
    print(accuracy)

    exit()
            

    
    dataset1 = generate_sin_wave(100, 9000, amp1, freq1, phase1)
    
    cav_model = tcav._linear_model(len(X_train[0]), 10, X_train, Y_train)
    accuracy = tcav.benchmark(cav_model, X_test, Y_test)

    ''' amplitude concept '''

    ''' phase concept '''
