import torch
import numpy as np
import model as mod
import torch.optim as optim
import torch.nn as nn
import copy

class TextDoc:

    def __init__(self, textfile):
        # data I/O
        self.data = open(textfile, 'r').read() # should be simple plain text file
        self.chars = list(set(self.data))
        data_size, vocab_size = len(self.data), len(self.chars)
        print('data has %d characters, %d unique.' % (data_size, vocab_size))
        self.char_to_ix = { ch:i for i,ch in enumerate(self.chars) }
        self.ix_to_char = { i:ch for i,ch in enumerate(self.chars) }

    def string_to_index(self, string):
        return [self.char_to_ix[ch] for ch in string]

    def index_to_string(self, index_list):
        return ''.join(self.ix_to_char[ix] for ix in index_list)

    def get_string_segment(self, start_idx, end_idx):
        return self.data[start_idx:end_idx]

    def get_idx_segment(self, start_idx, end_idx):
        return self.string_to_index(self.get_string_segment(start_idx, end_idx))

def one_hot(xs, vocab_size):
    output = torch.zeros((len(xs), 1, vocab_size))
    for i in range(len(xs)):
        output[i][0][xs[i]] = 1
    return output
    

def sample(model, seed, n, vocab_size, param):
    x = torch.tensor([seed], dtype=torch.long).cuda()
    ixes = []
    with torch.no_grad():
        # copy the model to cpu
        for i in range(n):

            scores = model.sample_forward(x)
            ix = np.random.choice(range(vocab_size), p=scores.cpu().numpy().ravel())
            x = torch.tensor([ix], dtype=torch.long).cuda()
            ixes.append(ix)
    return ixes

if __name__ == "__main__":
    # hyperparameters
    hidden_size = 100 # size of hidden layer of neurons
    seq_length = 25 # number of steps to unroll the RNN for
    learning_rate = 1e-4
    n, p = 0, 0 

    dw = TextDoc("input.txt")
    vocab_size = len(dw.chars)

    # Model params
    param = {}
    param["nlayers"] = 3
    param["input_dim"] = vocab_size
    param["output_dim"] = vocab_size
    param["hidden_dim"] = 100
    param["embedding_dim"] = 10
    param["vocab_size"] = vocab_size
    param["epoch"] = 100
    param["path"] = "./tmp"
    param["timesteps"] = seq_length
    param["loss"]="cce"
    param["classification"]=True
    
    pred_model = mod.TaskPredictor(param["nlayers"],param["input_dim"], param["hidden_dim"], param["output_dim"], param["embedding_dim"], param["timesteps"])
    pred_model.cuda()

    optimizer = optim.Adam(pred_model.parameters(), lr=learning_rate)
    loss_function = nn.CrossEntropyLoss()
    
    while True:
        # prepare inputs (we're sweeping from left to right in steps seq_length long)
        if p+seq_length+1 >= len(dw.data) or n == 0: 
            # Reset RNN memory
            print("RESET")
            p = 0 # go from start of data
        inputs = dw.get_idx_segment(p,p+seq_length)
        targets = dw.get_idx_segment(p+1,p+seq_length+1)

        # sample from the model now and then
        if n % 100 == 0 :
            sample_ix = sample(pred_model, inputs[0], 200, vocab_size, param)
            txt = dw.index_to_string(sample_ix)
            print('----\n %s \n----' % (txt, ))

        # forward seq_length characters through the net and fetch gradient
        # smooth_loss = smooth_loss * 0.999 + loss * 0.001
        # if n % 100 == 0: print('iter %d, loss: %f' % (n, smooth_loss)) # print progress

        for i in range(len(inputs)):
            x = torch.tensor([inputs[i]], dtype=torch.long).cuda()
            y = torch.tensor([targets[i]]).cuda()
            # perform parameter update with Adagrad
            mod.train_single_cce_batch(pred_model, x, y, loss_function, optimizer, label_weights=None)


        p += seq_length # move data pointer
        n += 1 # iteration counter 
