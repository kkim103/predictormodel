import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import grad
import math
import random
import numpy as np

torch.manual_seed(1)

# nlayers = number of LSTM layers
# input_dim = the max length of input
# hidden_dim = dimention of the hidden state
# Must init hidden before using the model
class TaskPredictor(nn.Module):
    def __init__(self, nlayers, input_dim, hidden_dim, output_dim, cuda=False):
        super(TaskPredictor, self).__init__()
        self.hidden_dim = hidden_dim
        self.input_dim = input_dim
        self.nlayers = nlayers
        self.on_cuda = cuda

        self.output_dim = output_dim
        self.embeddings = nn.Embedding(input_dim, hidden_dim)
        self.rnn = nn.LSTM(hidden_dim, hidden_dim, nlayers)
        self.linear = nn.Linear(self.hidden_dim, self.output_dim)

        if self.on_cuda:
            self.embeddings = self.embeddings.cuda()
            self.rnn = self.rnn.cuda()
            self.linear = self.linear.cuda()

    def _zero_hidden(self, nlayers, batch, cuda=False):
        if cuda:
            return (torch.randn(nlayers, batch, self.hidden_dim).cuda(), torch.randn(nlayers, batch, self.hidden_dim).cuda())
        # The axes semantics are (num_layers, minibatch_size, hidden_dim)
        return (torch.randn(nlayers, batch, self.hidden_dim), torch.randn(nlayers, batch, self.hidden_dim))

    def init_hidden(self, batch, cuda=False):
        # The axes semantics are (num_layers, minibatch_size, hidden_dim)
        '''
        hidden = []
        for i in range(self.nlayers):
            hidden.append(self._zero_hidden(batch, cuda))
        return hidden
        '''
        return self._zero_hidden(self.nlayers, batch, cuda)

    # detatch the hidden after each forward
    def forward(self, x, hidden):
        batch = x.size()[0]
        output = self.embeddings(x).view(1, batch, -1)
        output, hidden = self.rnn(output, hidden)
        task_space = self.linear(output.view(batch,-1))
        task_scores = F.softmax(task_space, dim=1)
        return  task_space, hidden

    # second LSTM is the bottleneck
    def output_bottleneck(self,x, hidden):
        bottleneck = None
        output = []
        with torch.no_grad():
            batch = x.size(0)
            intput = self.embeddings(x).view(1, batch, -1)
            for i in range(self.nlayers):
                if i == 0:
                    o, hidden[i] = self.layers[i](input, hidden[i])
                    output.append(o)
                else:
                    o, hidden[i] = self.layers[i](o, hidden[i])
                    output.append(o)
            task_space = self.linear(output.view(batch,-1)) 
        return  o, task_space

    def output_tcavq(self, x, y, c, k, layer=-1):
        self.zero_grad()
        self.init_hidden()
        bottlenecks, y_pred = self.output_bottleneck(x)
        bottleneck = bottlenecks[layer]
        print(bottleneck.size())
        loss_function = nn.CrossEntropyLoss(None)
        # This makes no sense
        loss = loss_function(y_pred.squeeze(), torch.autograd.Variable(y.long()))
        print("printing gradient")
        # printout the result
        print(torch.dot(grad(loss, bottleneck)[0].squeeze()[-1].cpu(), c))
        return

    
def train_single_cce_batch(predictor, x, y, loss_function, optimizer, label_weights=None):
    predictor.zero_grad()
    predictions  = predictor(x)
    loss = loss_function(predictions, y)
    loss.backward()
    optimizer.step()
    predictor.detach_hidden()
    return loss

# example of optimizer = optim.SGD(model.parameters(), lr=0.1)
# trace_param is a list of things to keep track of
def train(predictor, XS, YS, epoch, timesteps, input_dim, label_weights=None, XV=None, YV=None, trace_param=None, loss_type="cce"):
    dataset_size = len(XS)
    if dataset_size != len(YS):
        raise Exception("Expected that the example length and the label length to be the same. But it is not")

    epoch_history = []
    optimizer = optim.Adam(predictor.parameters(), lr=0.001)
    if loss_type == "mse":
        loss_function = nn.MSELoss()
    else:
        loss_function = nn.CrossEntropyLoss(label_weights)

    for e in range(epoch):
        print("epoch " + str(e))
        train_log = {}
        train_log["train_loss"] = [0]
        predictor.drop = True
        i = 0
        while i < dataset_size-timesteps: 
            print(str(i)+"/"+str(dataset_size-timesteps), end="\r", flush=True)
            x = torch.stack(XS[i:i+timesteps]).view((timesteps,1,input_dim)).cuda()
            y = torch.stack(YS[i:i+timesteps]).view((timesteps)).cuda()
            train_single_cce_batch(predictor, x, y, loss_function, optimizer, label_weights)
            # i += 1
            i += timesteps
        print()

    predictor.drop = False
    return 

def _label_inverse_distribution(YT, label_size):
    output = torch.ones(label_size).cuda()
    for t in YT:
        y = int(t)
        output[y]+=1
    for i in range(label_size):
        output[i] = 1/(float(output[i])/len(YT))
    return output

def save_model(model, path):
    torch.save(model, path)

def load_model(path):
    return torch.load(path)

# the prediction has shape of (1, -1)
def _decision(predictions):
    _, idxs = predictions.max(0)
    return idxs


def _output_stat(Yp, Yt):
    # output confusion matrix
    for p, t in zip(Yp, Yt):
        # get intdex
        ap = p.max(0)[1]
        at = t.max(0)[1]

    return


def output_stat(Yp, Yt, label_size):
    # axis 0 = predicted, axis 1= truth
    confusion_matrix = np.zeros((label_size, label_size))

    # output confusion matrix
    for p, t in zip(Yp, Yt):
        confusion_matrix[int(p.item())][int(t.item())]+=1

    total_correct = 0
    for i in range(label_size):
        total_correct += confusion_matrix[i][i]

    print("Accuracy = " + str(float(total_correct)/len(Yp)))
    print(confusion_matrix)
    return

def output_stat_regression_mse(Yp, Yt):
    return (np.square(Yp - Yt)).mean(axis=0)



# output performance report
def seq_kfold_validation(k, XS, YS, save, param):
    np.set_printoptions(precision=3)
    chunk_size = math.floor(len(XS) / k)
    # sanity check
    unequal_example_length = len(XS) != len(YS)
    chunk_too_small = chunk_size < 1
    if unequal_example_length:
        raise ValueError("The examples (XS) and labels (YS) are not of equal length")
    if chunk_too_small:
        raise ValueError("Fold count is "+str(k)+" but the example length is "+str(len(XS))+". Therefore, each fold has less than 1 examples")

    # calculate the size of dataset for each chunk
    chunk_idx = [0] # the value 0 is in the array and the value only exists to make the basecase of the loop work
    for i in range(k):
        new_index = chunk_idx[-1] + chunk_size
        chunk_idx.append(new_index)
    chunk_idx.pop(0)
    chunk_idx.pop(-1)

    # load the parameter variables
    nlayers = param["nlayers"]
    input_dim = param["input_dim"]
    output_dim = param["output_dim"]
    hidden_dim = param["hidden_dim"]
    epoch = param["epoch"]
    path = param["path"]
    timesteps = param["timesteps"]
    loss_type = param["loss"]
    classification = param["classification"]

    # start training and saving and conditionally save the model
    # TODO: dont forget that the last chunk need to be the test set
    fold_count = 0
    for idx in chunk_idx:
        print("fold count "+str(fold_count))
        fold_count += 1
        X_chunk = XS[:idx]
        Y_chunk = YS[:idx]

        Xt_chunk = XS[idx:idx+chunk_size]
        Yt_chunk = YS[idx:idx+chunk_size]

        # trim excess batch
        trimmed_size = chunk_size - chunk_size%timesteps
        X_chunk = X_chunk[:trimmed_size]
        Y_chunk = Y_chunk[:trimmed_size]
        Xt_chunk = Xt_chunk[:trimmed_size]
        Yt_chunk = Yt_chunk[:trimmed_size]

        pred_model = TaskPredictor(nlayers, input_dim, hidden_dim, output_dim, timesteps)
        pred_model.cuda()
        if loss_type == "cce":
            label_weights = _label_inverse_distribution(Y_chunk, output_dim)
        else:
            label_weights = None
        train(pred_model, X_chunk, Y_chunk, epoch, timesteps, input_dim, label_weights=label_weights, loss_type=loss_type)

        if True:
            save_model(pred_model, path + "_" + str(idx))

        Yp = []

        with torch.no_grad():
            print("running test")
            # test the model
            test_set_size = 0
            for i in range(len(Xt_chunk)-timesteps):
                test_set_size += 1
                x = torch.stack(Xt_chunk[i:i+timesteps]).view((timesteps,1,input_dim)).cuda()
                prediction = pred_model(x).squeeze()[-1].cpu()
                if classification:
                    Yp.append(_decision(prediction))
                else:
                    Yp.append(prediction.item())
            if classification:
                output_stat(torch.stack(Yp), Yt_chunk, output_dim)
            else:
                result = output_stat_regression_mse(np.array(Yp), np.array(list(map(lambda x: x.item(),Yt_chunk[:len(Xt_chunk)-timesteps]))))
                print(result)
                    
        return pred_model

    return
