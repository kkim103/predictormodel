import model
import torch

class TaskTrace:

    # TODO: create vectors with timer inforamtion
    # TODO: also output the value of longest duration of any task so that
    #the input is between the value of  0 and 1
    # get all the necessary metadata
    def __init__(self, file_dir, device):
        # store raw trace in memory first
        self.trace = self._read_trace_file(file_dir)
        self.label_size = self._detect_label_card()
        self.is_cuda = device == 'cuda'

    def _read_trace_file(self,fileName):
        data_file = open(fileName, 'r')
        sequences = data_file.readlines()[0].split(',')
        trace = list(map(int, list(map(str.strip,sequences))))
        return trace

    '''
        This function assumes that labels are in a integer sequence from 0 to n. No skipping of a seuqence.
    '''
    def _detect_label_card(self):
        dic = {}
        for tr in self.trace:
            dic[str(tr)]=1
        return len(dic)

    '''
        Default dataset preperation
    '''
    def prepare_dataset(self, offset):
        return self._prepare_dataset(1, offset)
        


    '''
        input is of shape (time_steps, label_card)
        output is of shape (time_steps, label_card)
        Visual Representation (X = left column, Y = right column)
        t -           l tstepsOffset+
        s  -          c  tstepsOffset+
        t   -         a   tstepsOffset+
        e    -        r    tstepsOffset+
        p     -       d     tstepsOffset+
        s      -
    '''
    # now outputs a series of x and series of y where y is x with an offset
    @torch.no_grad()
    def _prepare_dataset(self, time_steps, offset):
        list_of_examples = []
        list_of_labels = []
        example_shape = (time_steps,self.label_size)
        for i in range(len(self.trace)):
            if i + time_steps+offset+self.label_size >= len(self.trace):
                break

            # squash the mutiple steps
            example_datum = self._prepare_zero_tensor(example_shape)
            for j in range(time_steps):
                task_num = self.trace[j+i]
                example_datum[j][int(task_num)] = 1
            list_of_examples.append(example_datum)

            label_datum = self._prepare_zero_tensor((time_steps))
            for j in range(time_steps):
                task_num = self.trace[i + offset + j] # removed "+ timesteps"
                label_datum[j] = task_num
            list_of_labels.append(label_datum)
        return (list_of_examples, list_of_labels)

    @torch.no_grad()
    def _prepare_zero_tensor(self, shape):
        if self.is_cuda:
            return torch.zeros(shape, requires_grad=True).cuda()
        return torch.zeros(shape)

    def _prepare_random_tensor(self, shape):
        if self.is_cuda:
            return torch.randn(1, shape).cuda()
        return torch.randn(1, shape)




if __name__ == '__main__':
    device = "cuda"
    data_file = "/home/kyo/Workspace/Data/ExampleData.data"
    dataset_handler=TaskTrace(data_file, device)
    ds = dataset_handler.prepare_dataset(10)
    XS = ds[0]
    # XS = XS[:int(len(XS)/4)]
    YS = ds[1]
    # YS = YS[:int(len(YS)/4)]

    param = {}
    param["nlayers"] = 2
    param["input_dim"] = dataset_handler.label_size
    param["output_dim"] = dataset_handler.label_size
    param["hidden_dim"] = 100
    param["epoch"] = 1
    param["path"] = "./tmp"
    param["mini_batch"] = 1000

    if False:
        model.seq_kfold_validation(2, XS, YS, False, param,device)
    
