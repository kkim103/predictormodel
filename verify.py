import torch
from torch.autograd import Variable
import parser.tasktrace as pt
from model import TaskPredictor
import random
import unidecode
import string
import time
import math
import numpy as np
import torch.nn as nn

def generate(decoder, ind_to_char, prime_str='A', predict_len=100, temperature=0.8, cuda=False):
    hidden = decoder.init_hidden(1,cuda=cuda)
    prime_input = Variable(char_tensor(prime_str).unsqueeze(0))

    if cuda:
        prime_input = prime_input.cuda()
    predicted = prime_str

    # Use priming string to "build up" hidden state
    for p in range(len(prime_str) - 1):
        _, hidden = decoder(prime_input[:,p], hidden)
        
    inp = prime_input[:,-1]
    
    for p in range(predict_len):
        output, hidden = decoder(inp, hidden)
        
        # Sample from the network as a multinomial distribution
        output_dist = output.data.view(-1).div(temperature).exp()
        top_i = torch.multinomial(output_dist, 1)[0]

        # Add predicted character to string and use as next input
        predicted_char = ind_to_char[top_i.item()]
        predicted += predicted_char
        inp = Variable(char_tensor(predicted_char).unsqueeze(0))
        if cuda:
            inp = inp.cuda()

    return predicted


# Reading and un-unicode-encoding data

all_characters = string.printable
n_characters = len(all_characters)

def read_file(filename):
    file = unidecode.unidecode(open(filename).read())
    return file, len(file)

# Turning a string into a tensor


# Readable time elapsed

def time_since(since):
    s = time.time() - since
    m = math.floor(s / 60)
    s -= m * 60
    return '%dm %ds' % (m, s)


if __name__ == "__main__":
    data = open('shakespeare.txt', 'r').read() # should be simple plain text file
    #output = pt.spaceIntervals(data, "0")
    output = list(map(str, data))
    chars = list(set(output))
    data_size, vocab_size = len(output), len(chars)
    print('data has %d characters, %d unique.' % (data_size, vocab_size))
    char_to_ix = { ch:i for i,ch in enumerate(chars) }
    ix_to_char = { i:ch for i,ch in enumerate(chars) }

    def char_tensor(string):
        tensor = torch.zeros(len(string)).long()
        for c in range(len(string)):
            try:
                tensor[c] = char_to_ix[string[c]]
                #tensor[c] = all_characters.index(string[c])
            except:
                continue
        return tensor

    def random_training_set(data, chunk_len, batch_size, cuda):
        inp = torch.LongTensor(batch_size, chunk_len)
        target = torch.LongTensor(batch_size, chunk_len)
        for bi in range(batch_size):
            start_index = random.randint(0, len(data) - chunk_len-1)
            end_index = start_index + chunk_len + 1
            chunk = data[start_index:end_index]
            inp[bi] = char_tensor(chunk[:-1])
            target[bi] = char_tensor(chunk[1:])
        inp = Variable(inp)
        target = Variable(target)
        if cuda:
            inp = inp.cuda()
            target = target.cuda()
        return inp, target

    #############
    # hyper param
    #############
    nlayers = 2
    input_dim = vocab_size
    hidden_dim = 100
    output_dim = vocab_size
    embedding_dim = vocab_size
    seq_length = 200 # number of steps to unroll the RNN for
    window = 25
    learning_rate = 0.01
    cuda = True
    batch_size = 1
    epoch = 2000


    model = TaskPredictor(nlayers, input_dim, hidden_dim, output_dim, cuda)
    optimiser = torch.optim.Adam(model.parameters(), lr=learning_rate)
    loss_fn=nn.CrossEntropyLoss()


    index_data = [ char_to_ix[i] for i in output]

    #####################
    # Train model
    #####################

    hist = np.zeros(epoch)

    smooth_loss = -np.log(1.0/vocab_size)*seq_length # loss at iteration 0
    for t in range(epoch):


        ############
        # sample
        ############
        if t%100 == 0:
            print(generate(model, ix_to_char, 'CLEO', 100, cuda=cuda), '\n')

        # Initialise hidden state
        # Don't do this if you want your LSTM to be stateful

        hidden = model.init_hidden(batch_size, cuda=cuda)
        inp, target = random_training_set(data, seq_length, batch_size, cuda)

        p = 0
        while p < seq_length:
            loss_acc = 0
            # Clear stored gradient
            model.zero_grad()
            loss = 0
            for i in range(window):
                x = inp[:,i]
                y = target[:,i]
                
                # Forward pass
                y_pred, hidden = model(x, hidden)
                loss += loss_fn(y_pred, y)

                loss_acc += loss.item()

            # Backward pass
            loss.backward()
            # Update parameters
            optimiser.step()
            smooth_loss = smooth_loss * 0.999 + loss_acc * 0.001

            if p/25 % 100 == 0:
                print("Epoch ", t, "MSE: ", smooth_loss)
            hist[t] = smooth_loss
            loss_acc = 0
            
            p += seq_length


