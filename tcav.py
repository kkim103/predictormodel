import torch
from mnist import MNIST
import torch.optim as optim

def _linear_model(input_dim, output_dim, XcS, YcS):
    # snippets from pytorch example file
    cav_model = torch.nn.Sequential(torch.nn.Linear(input_dim, output_dim)).cuda()
    loss_fn = torch.nn.CrossEntropyLoss(None)
    optimizer = optim.Adam(cav_model.parameters(), lr=0.001)
    total_size = len(XcS)
    prog_unit = int(total_size * 0.05)
    for ep in range(20):
        print("epoch " + str(ep))
        i = 0
        prog = 0
        for x, y in zip(XcS, YcS):
            i += 1
            if i % prog_unit == 0:
                prog += 1
                i = 0
                print(str(5 * prog), end="\r", flush=True)
            x = torch.cuda.FloatTensor(x).view((1,-1))
            temp = torch.zeros(1).cuda()
            temp[0] = y
            y = temp
            yp = cav_model(x)
            loss = loss_fn(yp, torch.autograd.Variable(y.long()))
            cav_model.zero_grad()
            loss.backward()
            optimizer.step()
        print("")
    return cav_model

def _extract_weights(model):
    print(model.parameters())
    return

def benchmark(model, X_test, Y_test):
    total_size = len(X_test)
    correct = 0
    for x, y in zip(X_test, Y_test):
        x = torch.cuda.FloatTensor(x)
        _, yp = model(x).max(0)
        if yp == y:
            correct += 1
    return float(correct)/total_size

if __name__ == "__main__":
    # verify linear model implementation with mnist dataset
    mndata = MNIST('./mnist_data')
    X_train, Y_train = mndata.load_training()
    X_test, Y_test = mndata.load_testing()
    print(len(Y_train))
    cav_model = _linear_model(len(X_train[0]), 10, X_train, Y_train)
    accuracy = benchmark(cav_model, X_test, Y_test)
    print(accuracy)
